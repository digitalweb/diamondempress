//Prince Blessing Sunday don come again. libertylivingstone@yahoo.com
//javascript for reeservation (requires jQuery)

//Global Variables
var wmodalwin = null;
var timeoutID = null;
var rsv_errors = "";
var ajaxSetupDone = false;

//btnSubmit
$(function(){	
	$('form[name*=frmReservation]').submit(function(){
		return validate_form(this);
	});
	
	setupCalendars();
});

function setupCalendars(){
	//Check in date calendar
	//Some pars %B %e, %Y
	Calendar.setup({
		inputField 	: 	"rcheckindate",
		ifFormat 	: 	"%Y/%m/%d",
		displayArea : 	"rcheckindate_display",
		daFormat 	: 	"%a, %B %d, %Y",
		button 		: 	"rcheckindate_tg",
		align		:	"Br"
	});
	
	//Check out date calendar
	Calendar.setup({
		inputField 	: 	"rcheckoutdate",
		ifFormat 	: 	"%Y/%m/%d",
		displayArea : 	"rcheckoutdate_display",
		daFormat 	: 	"%a, %B %d, %Y",
		button 		: 	"rcheckoutdate_tg",
		align		:	"Br"
	});
}

function validate_form(frm){
	//alert('submitting process, Reserving.....' + frm.name);	
	rsv_errors = "";
	
	if ( isBlank(frm.rname.value) ){
		rsv_errors = "Please enter your name first";
		frm.rname.focus();
	}
	else if( isBlank(frm.remail.value) ){
		rsv_errors = "Please enter a valid email address";
		frm.remail.focus();
	}
	else if( !isValidEmail(frm.remail.value) ){
		rsv_errors = "Invalid email address entered!";
		frm.remail.focus(); frm.remail.select();
	}
	else if (!isBlank(frm.rphone.value) && !isValidPhoneNumber(frm.rphone.value)){
		rsv_errors = "Your telephone number must be made up of numbers alone (Minimum of 6 digits)";
		frm.rphone.focus(); frm.rphone.select();
	}
	else if (!isBlank(frm.rrooms.value) && !isValidQuantity(frm.rrooms.value)){
		rsv_errors = "Please enter a valid number of rooms to reserve";
		frm.rrooms.focus(); frm.rrooms.select();
	}
	else if (!isBlank(frm.rkids.value) && !isValidQuantity(frm.rkids.value)){
		rsv_errors = "Please enter a valid number of children";
		frm.rkids.focus(); frm.rkids.select();
	}
	else if (!isBlank(frm.radults.value) && !isValidQuantity(frm.radults.value)){
		rsv_errors = "Please enter a valid number of adults";
		frm.radults.focus(); frm.radults.select();
	}
	else if ( isBlank(frm.rcheckindate.value) ){
		rsv_errors = "Please select a check in date";
	}
	else if ( isBlank(frm.rcheckoutdate.value) ){
		rsv_errors = "Please select a check out date";
	}
	else if ( !validReserveDates(frm.rcheckindate.value, frm.rcheckoutdate.value) ){
		//check date consistency
		//All errors are displayed in the function itself!
	}
	
	(rsv_errors == "") ? triggerWRQS(frm) : showError(rsv_errors);
	return false;
}

function validReserveDates(cinDate, coutDate){
	//V-alidation to ensure check in date is less than check out date.	
	var datecheck = compareDates(cinDate, coutDate);
	if (datecheck == 0){
		rsv_errors = ""; //Check in and out the same day! No error sha
	}
	else if (datecheck == 1){
		rsv_errors = "Your check out date cannot come before check in date. Please re-select them";
	}
	else if(datecheck == -1){
		//This is OK, No need to panic cos date1 (check in) < date 2 (check out)
	}
	else {
		rsv_errors = "Either check in or check out date is invalid!";
	}
	
	if (rsv_errors == ""){
		return true;
	}
	else {
		showError(rsv_errors);
		return false;
	}
}

function sendReservation(frm){
	//Using Ajax, Send the reservation
	//space for information id = rsv_info
	//var infoid = "rsv_info";
	var rsv_data = $('form[name*=frmReservation]').serialize();
	//alert(rsv_data);
	$.post(
		'winchesterRQS/wRQS.php',
		rsv_data,
		function(data, status) { 
			//alert("Status: " + status + " and  data : "  + data);
			if (status.toLowerCase() == "success"){
				wmodalwin.load('inline', getWindowContent("success", data), 'Winchester Online Reservation');
				frm.reset();
				//$('#rcheckindate_display').html("Select Check in date");
				//$('#rcheckoutdate_display').html("Select Check out date");
			}
			else if(status.toLowerCase() == "failure"){
				wmodalwin.load('inline', getWindowContent("failure", data), 'Winchester Online Reservation');
			}
			else{
				wmodalwin.load('inline', getWindowContent("failure", data), 'Winchester Online Reservation');
			}
		}
	);
}

function triggerWRQS(frm){ 
	//alert("almost"); return;
	//var uniquevar=dhtmlwindow.open(uniqueID, contenttype, contentsource, title, attributes, [recalonload])
	var windowcontent = getWindowContent("sending");
	wmodalwin = dhtmlmodal.open('wrqswin', 'inline', windowcontent, 'Winchester Online Reservation',
								'width=500px,height=50px,center=1,resize=0,scrolling=1');
	sendReservation(frm);
}

function getWindowContent(wcase, content){
	var wcontent = "";
	
	if (wcase.toLowerCase() == "sending"){
		wcontent =  '<div id="reservediv" class="winrsvdiv">' +
						'<p style="margin:0px;">&nbsp;</p>' +
						'<img src="images/loading.gif" border="0" title="" />' +
						'<p style="margin:0px;">&nbsp;</p>' +
						'<span id="rsv_info" class="wininfo">' +
							'Please wait while your reservation is placed...' +
						'</span>' +
					'</div>';
	}
	else if(wcase.toLowerCase() == "success"){
		wcontent = '<div id="reservediv" class="winrsvdiv winblacktext">' +
						'<p style="margin:0px;">&nbsp;</p>' +
						'<span id="rsv_info" class="wininfo">'
							+ content +
						'</span>' +
					'</div>';
	}
	else{
		wcontent = '<div id="reservediv" class="winrsvdiv winredtext">' +
						'<p style="margin:0px;">&nbsp;</p>' +
						'<span id="rsv_info" class="wininfo">'
							+ content +
						'</span>' +
					'</div>';
	}
	
	//alert("wcase: " + wcase + "\nWindow content sending:\n " + wcontent);
	return wcontent;
}

function isBlank(s) {
	// Check if string is non-blank
	var isBlank_re = /\S/;
	return String (s).search (isBlank_re) == -1
}

function isValidEmail(email) {
	var isEmail_re = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
	return String(email).search (isEmail_re) != -1; // if it is == -1, it failed!
}

function isValidPhoneNumber(phone) {
	var isPhone_re = /^\d{6,}$/;
	return String(phone).search (isPhone_re) != -1; // if it is == -1, it failed!
}

function isValidQuantity(qty) {
	var isQty_re = /^\d{1,}$/;
	return String(qty).search (isQty_re) != -1; // if it is == -1, it failed!
}

//Compares two dates and returns 0 for EQ, 1 for date1 > date2, -1 for date2 > date2, null for invalid dates
//TO DO -Check for invalid dates
function compareDates(date1_value, date2_value){
	//alert("In compareDates fn, dat1, dat2 == " + date1_value + " :: " + date2_value);
	if (!date1_value || !date2_value) { return null; }
	var date1 = new Date(date1_value).getTime();
	var date2 = new Date(date2_value).getTime();
	return (date1 === date2)? 0 : ((date1 > date2) ? 1 : -1);
}

function showError(error){
	//Show the current validation error
	if (timeoutID != null){
		clearTimeout(timeoutID);
	}
	$('#rsv_errspace').html(error).show('slow');
	timeoutID = setTimeout("clearError()", 4000);
}

function clearError(){
	//Clear the current validation error
	$('#rsv_errspace').hide('slow').html("");
}
