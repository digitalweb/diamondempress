<?php

	require_once("class.phpmailer.php");
	
	class wRQS {
		
		//Main reservation recipient
		static $mail_to = "reservations@winchestersuites.com";
		
		//Separate multiple copy to addresses with comma (e.g a@b.com, c@d.com, e@f.com)
		static $copy_to = array(
			"blessing@dw.com.ng","omijeh@dw.com.ng","mamamanday1@yahoo.com","simeonoluyemi@yahoo.com","olanuyi@gmail.com",
			"ahonsii@yahoo.com","ooshomah@yahoo.com","mdyovo@yahoo.com"
		);
		
		static function mailReservation($rsv_data){
			//Mail a reservation request
			
			$rname = ""; $remail = ""; $rphone = ""; $rrooms = "";
			$rkids = ""; $radults = ""; $rcindate = ""; $rcoutdate = "";
			
			$rdata = "";
		
			$rname = $rsv_data["rname"];
			$remail = $rsv_data["remail"];
			$rphone = (!isset($rsv_data["rphone"]) || ($rsv_data["rphone"] == ""))? "unspecified" : $rsv_data["rphone"];
			$rrooms = (!isset($rsv_data["rrooms"]) || ($rsv_data["rrooms"] == ""))? "unspecified" : $rsv_data["rrooms"];
			$rkids = (!isset($rsv_data["rkids"]) || ($rsv_data["rkids"] == ""))? "unspecified" : $rsv_data["rkids"];
			$radults = (!isset($rsv_data["radults"]) || ($rsv_data["radults"] == ""))? "unspecified" : $rsv_data["radults"];
			$rcindate = $rsv_data["rcheckindate"];
			$rcoutdate = $rsv_data["rcheckoutdate"];
			$rsvdate = date("D, F j, Y \a\\t g:i a");
			
			//$info = $rname ." ". $remail ." ". $rphone ." ". $rrooms ." ". $rkids ." ". $radults ." ". $rcindate ." ". $rcoutdate;
			//return $info;

			$mail = new PHPMailer();
			$mail->IsHTML(true); //telling the class that it is an html mail
			$mail->IsSMTP();  // telling the class to use SMTP
			$mail->Host = "mail.winchestersuites.com"; // SMTP server
			$mail->Username = "winchestermailer+winchestersuites.com";
			$mail->Password = "winchestermailer";
			//$mail->Port = 465;

			$mail->From     = "reservations@winchestersuites.com";
			$mail->FromName = "Winchester Online Reservation";
			$mail->AddAddress(wRQS::$mail_to);
			foreach (wRQS::$copy_to as $cckey => $ccval) {
				$mail->AddAddress($ccval);
			}
			

			$mail->Subject  = "Online Reservation";
			$mailbody = "<div style=\"font-family:verdana; font-size:12px;\">" .
						"=============================================="."<br />".
						" <span align='center'>Winchester Online Reservation</span>"."<br />".
						"=============================================="."<br />".
						"<table border='0' style=\"font-family:verdana; font-size:12px;\">".
						"<tr><td>Name of Guest:</td><td>$rname"."</td></tr>".
						"<tr><td>Guest Email:</td><td> $remail"."</td></tr>".
						"<tr><td>Guest Phone:</td><td> $rphone"."</td></tr>".
						"<tr><td>Number of Rooms:</td><td> $rrooms"."</td></tr>".
						"<tr><td>Number of Kids:</td><td> $rkids"."</td></tr>".
						"<tr><td>Number of Adults:</td><td> $radults"."</td></tr>".
						"<tr><td>Check in Date:</td><td> $rcindate"."</td></tr>".
						"<tr><td>Check Out Date:</td><td> $rcoutdate"."</td></tr>".
						"<tr><td>Reservation made on:</td><td> $rsvdate"."</td></tr>".
						"</table>" .
						"=============================================="."<br />" .
						"</div>";
						
			//$mail->Body = $mailbody;
			$mail->MsgHTML($mailbody);
			$mail->AltBody = "This is an automated mail sent to notify you of a reservation made from your website. " .
							"To view this message, please use an HTML compatible email viewer!";
			$mail->WordWrap = 50;
			try {
				$rsvinfo = "";
				if(!$mail->Send()) {
					$rsvinfo = sprintf("<span class=\"winredtext\">Sorry, your reservation could not be placed. " .
											"<p style=\"margin:0px;\">&nbsp;</p>Please try again.");
				} 
				else {
					$rsvinfo = sprintf("Your reservation has been successfully placed. " .
											"<p style=\"margin:0px;\">&nbsp;</p>You will be contacted soon.");
				}
			}
			catch (Exception $ex){
				$rsvinfo = sprintf("<span class=\"winredtext\">Sorry, your reservation could not be placed. " .
											"<p style=\"margin:0px;\">&nbsp;</p>Please try again.");
			}
			
			return $rsvinfo;
		}
			
	}
	
	//Now write the actual posting code
	$info = "";
	$info = wRQS::mailReservation($_REQUEST);
	
	//Ajax Response
	echo $info

?>